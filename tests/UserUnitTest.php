<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setNom('nom')
            ->setPrenom('prenom')
            ->setEmail('true@email.com')
            ->setTelephone('telephone')
            ->setAdresse('adresse')
            ->setAge(666)
            ->setCodePostal('45678')
            ->setAPropos('a propos')
            ->setVille('ville')
            ->setGenre('genre')
            ->setPassword('password');


        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getEmail() === 'true@email.com');
        $this->assertTrue($user->getTelephone() === 'telephone');
        $this->assertTrue($user->getAdresse() === 'adresse');
        $this->assertTrue($user->getAge() === 666);
        $this->assertTrue($user->getCodePostal() === '45678');
        $this->assertTrue($user->getAPropos() === 'a propos');
        $this->assertTrue($user->getVille() === 'ville');
        $this->assertTrue($user->getGenre() === 'genre');
        $this->assertTrue($user->getPassword() === 'password');

    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setNom('nom')
            ->setPrenom('prenom')
            ->setEmail('true@email.com')
            ->setTelephone('telephone')
            ->setAdresse('adresse')
            ->setAge(666)
            ->setCodePostal('45678')
            ->setAPropos('a propos')
            ->setVille('ville')
            ->setGenre('genre')
            ->setPassword('password');


        $this->assertFalse($user->getNom() === 'falsenom');
        $this->assertFalse($user->getPrenom() === 'falseprenom');
        $this->assertFalse($user->getEmail() === 'false@email.com');
        $this->assertFalse($user->getTelephone() === 'falsetelephone');
        $this->assertFalse($user->getAdresse() === 'afalsedresse');
        $this->assertFalse($user->getAge() === 667);
        $this->assertFalse($user->getCodePostal() === 'false45678');
        $this->assertFalse($user->getAPropos() === 'falsea propos');
        $this->assertFalse($user->getVille() === 'falseville');
        $this->assertFalse($user->getGenre() === 'falsegenre');
        $this->assertFalse($user->getPassword() === 'falsepassword');

    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getAdresse());
        $this->assertEmpty($user->getAge());
        $this->assertEmpty($user->getCodePostal());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getVille());
        $this->assertEmpty($user->getGenre());
        $this->assertEmpty($user->getPassword());

    }


}
